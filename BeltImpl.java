package a7;

import java.util.ArrayList;

import a7.PlateEvent.EventType;
import comp401sushi.Plate;


public class BeltImpl implements Belt {
	private ArrayList<BeltObserver> BeltObservers = new ArrayList<BeltObserver>();
	private Plate[] _belt;
	private Customer[] customers;

	public BeltImpl(int belt_size) {
		if (belt_size < 1) {
			throw new IllegalArgumentException("Illegal belt size"); 
		}
		_belt = new Plate[belt_size];
		customers = new Customer[belt_size];
	}

	// returns this belts length
	public int getSize() {
		return _belt.length;
	}

	// returns plate at given position
	public Plate getPlateAtPosition(int position) {
		position = normalize_position(position);
		return _belt[normalize_position(position)];
	}

	// sets Plate plate at given position
	public void setPlateAtPosition(Plate plate, int position) throws BeltPlateException {
		if (plate == null) {
			throw new IllegalArgumentException();
		}
		position = normalize_position(position);
		if (getPlateAtPosition(position) != null) {
			throw new BeltPlateException(position, plate, this);
		}
		notifyObservers(new PlateEvent (EventType.PLATE_PLACED, plate, position));

		_belt[position] = plate;
	}

	// clears plate at given position
	public void clearPlateAtPosition(int position) {
		position = normalize_position(position);
		notifyObservers(new PlateEvent (EventType.PLATE_REMOVED, _belt[position], position));
		_belt[position] = null;		

	}

	// returns a normilized position that is a valid belt postion 
	private int normalize_position(int position) {
		int size = getSize();
		return (((position % size) + size) % size);
	}

	// returns the positon of the next empty belt position and sets Plate plate to that position
	public int setPlateNearestToPosition(Plate plate, int position) throws BeltFullException {
		int offset = 0;
		position = normalize_position(position);
		while (offset < getSize()) {
			try {
				setPlateAtPosition(plate, position+offset);
				return normalize_position(position+offset);
			}
			catch (BeltPlateException e) {
				offset += 1;
			}
		}
		throw new BeltFullException(this);
	}

	// moves every plate on this belt to the next position
	public void rotate() {
		Plate last_plate = _belt[getSize()-1];
		for (int i=getSize()-1; i>0; i--) {
			_belt[i] = _belt[i-1];
		}
		_belt[0] = last_plate;
		for (int i = 0; i < customers.length; i++) {
			if (customers[i] != null && _belt[i] != null) {
				customers[i].observePlateOnBelt(this, _belt[i], i);
			}
		}	
	}

	// adds BeltObserver o to the BeltObservers list
	public void addBeltObserver(BeltObserver o) {	
			BeltObservers.add(o);
		
	}

	// removes BeltObserver o to the BeltObservers list
	public void removeBeltObserver(BeltObserver o) {
			BeltObservers.remove(o);
	}

	// notfies all BeltObservers when a new PlateEvent happens
	public void notifyObservers (PlateEvent e) {
		for (BeltObserver b : BeltObservers) {
			b.handlePlateEvent(e);
		}
	}

	// registers Customer c to the given position on the BeltObservers list
	public void registerCustomerAtPosition(Customer c, int position) {
		int normalized_position = ((position % customers.length) + customers.length) % customers.length;
		if (c == null) {
			throw new IllegalArgumentException();
		}
		if (customers[normalized_position] != null) {
			throw new RuntimeException();
		}
		for (int i = 0; i < customers.length; i++) {
			if (customers[i] == c) {
				throw new RuntimeException();
			}
		}
		customers[normalized_position] = c;
	}

	// unregisters Customer c to the given position on the BeltObservers list
	public Customer unregisterCustomerAtPosition(int position) {
		int normalized_position = ((position % customers.length) + customers.length) % customers.length;
		if (customers[normalized_position] == null) {
			return null;
		} else {
			Customer removedCustomer = customers[normalized_position];
			customers[normalized_position] = null;
			return removedCustomer;
		}
	}
}
