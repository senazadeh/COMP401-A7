package a7;

import comp401sushi.Plate;

public class PlateEvent {
	public enum EventType {PLATE_PLACED, PLATE_REMOVED};
	
	private EventType type;
	private Plate plate;
	private int position;
	
	public PlateEvent(EventType type, Plate plate, int position) { 
		this.type = type;
		this.plate = plate;
		this.position = position;
	}
	
	// returns this.type
	public EventType getType() {
		return type;
	}
	
	// returns this.plate
	public  Plate getPlate() {
		return plate;
	}
	
	// returns this.postion 
	public int getPosition() {
		return position;
	}
}
