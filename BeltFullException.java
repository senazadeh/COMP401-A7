package a7;

public class BeltFullException extends Exception {
	private Belt _belt;
	
	// BeltFullException thrown if attempted to place a plate on a full belt
	public BeltFullException(Belt belt) {
		super("Belt full");
		_belt = belt;
	}
	
	// returns this belt
	public Belt getBelt() {
		return _belt; 
	}
}
