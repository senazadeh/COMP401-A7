package a7;

import a7.PlateEvent.EventType;
import comp401sushi.Plate;

// counts the number of each plate color located on the belt
public class PlateCounter implements BeltObserver {
	private int red = 0;
	private int green = 0;
	private int blue = 0;
	private int gold = 0; 
	private Belt b;
	
	public PlateCounter(Belt b) {
		if (b == null) { throw new IllegalArgumentException(); }
		this.b = b;
		this.b.addBeltObserver(this);
		
		for (int i = 0; i < this.b.getSize(); i++) {
			if (this.b.getPlateAtPosition(i) != null) {
				Plate.Color plateColor = this.b.getPlateAtPosition(i).getColor();
				switch (plateColor) {
					case RED:
						this.red++; 
						break;
					case GREEN:
						this.green++;
						break;
					case BLUE:
						this.blue++;
						break;
					case GOLD:
						this.gold++;
						break;
				}
			}
		}
	}

	// returns number of red plates on this belt
	public int getRedPlateCount() {
		return this.red;
	}
	// returns number of green plates on this belt
	public int getGreenPlateCount() {
		return this.green;
	}
	// returns number of blue plates on this belt
	public int getBluePlateCount() {
		return this.blue;
	}
	// returns number of gold plates on this belt
	public int getGoldPlateCount() {
		return this.gold;
	}
	
	// adds or subtracts a plate of Plate.Color when an event occurs 
	public void handlePlateEvent(PlateEvent e) {
			if (e.getType() == EventType.PLATE_PLACED) {
				Plate.Color eColor = e.getPlate().getColor();
				switch (eColor) {
				case RED:
					this.red++;
					break;
				case GREEN:
					this.green++;
					break;
				case BLUE:
					this.blue++;
					break;
				case GOLD:
					this.gold++;
					break;
				}
			} else {
				Plate.Color eColor = e.getPlate().getColor();
				switch (eColor) {
				case RED:
					this.red--;
					break;
				case GREEN:
					this.green--;
					break;
				case BLUE:
					this.blue--;
					break;
				case GOLD:
					this.gold--;
					break;
				}
			}
		}
	}

