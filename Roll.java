package a4;

import java.util.ArrayList;
import java.util.Arrays;

public class Roll implements Sushi{
	private String name = "";
	private IngredientPortion[] roll_ingredients = null;
	
	public Roll(String name, IngredientPortion[] roll_ingredients) {
	
		if (roll_ingredients == null) {
			throw new RuntimeException("roll_ingredients is null"); }
		for (int i = 0; i < roll_ingredients.length; i++) {
			if (roll_ingredients[i] == null) {
				throw new RuntimeException("index of roll_ingredients is null"); }}
	
	// Combines all portions together
	for (int i = 0; i < roll_ingredients.length; i++) {
		for (int j = i + 1; j < roll_ingredients.length; j++) {
			if (roll_ingredients[i] != null && roll_ingredients[j] != null) {
				if (roll_ingredients[i].getName().equals(roll_ingredients[j].getName())) {
					roll_ingredients[i] = roll_ingredients[i].combine(roll_ingredients[j]);
					roll_ingredients[j] = null;
				}
			}
		}
	}
	ArrayList<IngredientPortion> removedNull = new ArrayList<IngredientPortion>();
	for (int i = 0; i < roll_ingredients.length; i++) {
		if (roll_ingredients[i] != null) {
			removedNull.add(roll_ingredients[i]);
		}
	}
	IngredientPortion[] combined = new IngredientPortion[removedNull.size()];
	removedNull.toArray(combined);
	
	
	
	
	// Checks to see if SeaweedPortion exists 
	boolean hasSeaweed = false;
	for (int i = 0; i < combined.length; i++) {
		if (combined[i].getName().equals("seaweed")) {
			hasSeaweed = true;
			if (combined[i].getAmount() < 0.1) {
				combined[i] = new SeaweedPortion(0.1);
			}
		}
	}
	this.roll_ingredients = combined;
	if (hasSeaweed == false) {
		IngredientPortion[] addedSeaweed = new IngredientPortion[combined.length + 1];
		for (int i = 0; i < combined.length; i++) {
			addedSeaweed[i] = combined[i];
		}
		addedSeaweed[combined.length] = new SeaweedPortion(0.1);
		this.roll_ingredients = addedSeaweed;
	}
	
	
	
	
	
	
	this.name = name;
}

	
	public String getName() {
		return this.name;
	}

	public IngredientPortion[] getIngredients() {
		return this.roll_ingredients.clone();
	}

	public int getCalories() {
		double totalCalories = 0;
		for (int i = 0; i < this.roll_ingredients.length; i++) {
			totalCalories += roll_ingredients[i].getCalories();
		}
		int rounded = (int) (totalCalories + 0.5);
		return rounded;
	}

	public double getCost() {
		double totalCost = 0.00;
		for (int i = 0; i < this.roll_ingredients.length; i++) {
			totalCost += roll_ingredients[i].getCost();
		}
		return Math.round(totalCost * 100.0) / 100.0;
	}

	public boolean getHasRice() {
		boolean isRice = false;
		for (int i = 0; i < this.roll_ingredients.length; i++) {
			if (roll_ingredients[i].getIsRice() == true) {
				isRice = true;
			}
		}
		return isRice;
	}

	public boolean getHasShellfish() {
		boolean isShellfish = false;
		for (int i = 0; i < this.roll_ingredients.length; i++) {
			if (roll_ingredients[i].getIsShellfish() == true) {
				isShellfish = true;
			}
		}
		return isShellfish;
	}

	public boolean getIsVegetarian() {
		boolean isVegetarian = true;
		for (int i = 0; i < this.roll_ingredients.length; i++) {
			if (roll_ingredients[i].getIsVegetarian() == false) {
				isVegetarian = false;
			}
		}
		return isVegetarian;
	}

}
