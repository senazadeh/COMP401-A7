package a4;

public class AvocadoPortion implements IngredientPortion{
	private static Ingredient Avocado = new Avocado(); 
	private double amount = 0;
	
	public AvocadoPortion (double amount) {
		if (amount < 0) {
			throw new RuntimeException("Amount is less than 0"); }
	
	this.amount = amount; 
	}

	public Ingredient getIngredient() {
		return Avocado;
	}

	public String getName() {
		return Avocado.getName();
	}

	public double getAmount() {
		return amount;
	}

	public double getCalories() {
		return Avocado.getCaloriesPerOunce() * amount;
	}

	public double getCost() {
		return Avocado.getPricePerOunce() * amount;
	}

	public boolean getIsVegetarian() {
		return Avocado.getIsVegetarian();
	}

	public boolean getIsRice() {
		return Avocado.getIsRice();
	}

	public boolean getIsShellfish() {
		return Avocado.getIsShellfish();
	}

	public IngredientPortion combine(IngredientPortion other) {
		if (other == null) {
			return this; 
		} else if (!this.getName().equals(other.getName())) {
			throw new RuntimeException("Not the same ingregient"); 
		} else {
			AvocadoPortion combinedAvocado = new AvocadoPortion(this.getAmount() + other.getAmount());
			return combinedAvocado;
		}
	}	
}
