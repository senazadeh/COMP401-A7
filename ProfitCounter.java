package a7;

import a7.PlateEvent.EventType;

// counts the total profit of plates on the belt and the number of plates on the belt
public class ProfitCounter implements BeltObserver {
	private int numOfPlates = 0;
	private double profit = 0.0;
	private Belt b;

	public ProfitCounter(Belt b) {
		if (b == null) { throw new IllegalArgumentException(); }
		this.b = b;
		this.b.addBeltObserver(this);

		for (int i = 0; i < this.b.getSize(); i++) {
			if (this.b.getPlateAtPosition(i) != null) {
				this.numOfPlates++;
				this.profit += this.b.getPlateAtPosition(i).getProfit(); 
			}
		}
	}

	// returns the belts total profit
	public double getTotalBeltProfit() {
		System.out.print(this.profit);
		return this.profit;
	}
	// returns the average profit of each plate on the belt
	public double getAverageBeltProfit() {
		if (numOfPlates == 0) {
			return 0;
		}
		return this.profit / this.numOfPlates;
	}

	// adds or subtracts from total profit and plate number depending on PlateEvent e
	public void handlePlateEvent(PlateEvent e) {
		if (e.getType().equals(EventType.PLATE_PLACED)) {
			this.profit += e.getPlate().getProfit(); 
			numOfPlates++;
		} else {
			this.profit -= e.getPlate().getProfit();
			numOfPlates--;
		}
	}
}
