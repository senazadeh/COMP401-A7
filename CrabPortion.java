package a4;

public class CrabPortion implements IngredientPortion{
	private static Ingredient Crab = new Crab(); 
	private double amount = 0;
	
	public CrabPortion (double amount) {
		if (amount < 0) {
			throw new RuntimeException("Amount is less than 0"); }
	
	this.amount = amount; 
	}

	public Ingredient getIngredient() {
		return Crab;
	}

	public String getName() {
		return Crab.getName();
	}

	public double getAmount() {
		return amount;
	}

	public double getCalories() {
		return Crab.getCaloriesPerOunce() * amount;
	}

	public double getCost() {
		return Crab.getPricePerOunce() * amount;
	}

	public boolean getIsVegetarian() {
		return Crab.getIsVegetarian();
	}

	public boolean getIsRice() {
		return Crab.getIsRice();
	}

	public boolean getIsShellfish() {
		return Crab.getIsShellfish();
	}

	public IngredientPortion combine(IngredientPortion other) {
		if (other == null) {
			return this; 
		} else if (!this.getName().equals(other.getName())) {
			throw new RuntimeException("Not the same ingregient"); 
		} else {
			CrabPortion combinedCrab = new CrabPortion(this.getAmount() + other.getAmount());
			return combinedCrab;
		}
	}	
}
