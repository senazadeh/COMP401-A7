package a5;

public class GoldPlate implements Plate {
	private double price = 0.00;
	private Sushi contents = null;
	
	public GoldPlate(Sushi contents, double price) throws PlatePriceException {
		
		if (price < 5.0) {
			throw new IllegalArgumentException("price is less than 5"); }
		if (contents != null) {
			if (contents.getCost() <= price) {
				throw new PlatePriceException("price of plate and price of sushi dont match");
			}
		}
		
		this.contents = contents;
		this.price = price;
	}
	
	
	public Sushi getContents() {
		return this.contents;
	}

	public Sushi removeContents() {
		Sushi priorContents = this.contents;
		if (this.contents != null) {
			this.contents = null;
		}
		return priorContents;
	}

	public void setContents(Sushi s) throws PlatePriceException {
		if (s == null) {
			throw new IllegalArgumentException("s is null");
		} else if (this.price <= s.getCost()) {
			throw new PlatePriceException("no profit");
		} else {
		this.contents = s;
		}
	}

	public boolean hasContents() {
		if (this.contents == null) {
			return false;
		} else {
			return true;
		}
	}

	public double getPrice() {
		return this.price;
	}

	public Color getColor() {
		return Color.GOLD;
	}

	public double getProfit() {
		if (this.contents == null) {
			return 0.0;
		} else {
			double profit = this.price - contents.getCost() - 0.005;
			return Math.round(profit * 100.00) / 100.00;		}
	}

}
