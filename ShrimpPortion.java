package a4;

public class ShrimpPortion implements IngredientPortion{
	private static Ingredient Shrimp = new Shrimp(); 
	private double amount = 0;
	
	public ShrimpPortion (double amount) {
		if (amount < 0) {
			throw new RuntimeException("Amount is less than 0"); }
	
	this.amount = amount; 
	}

	public Ingredient getIngredient() {
		return Shrimp;
	}

	public String getName() {
		return Shrimp.getName();
	}

	public double getAmount() {
		return amount;
	}

	public double getCalories() {
		return Shrimp.getCaloriesPerOunce() * amount;
	}

	public double getCost() {
		return Shrimp.getPricePerOunce() * amount;
	}

	public boolean getIsVegetarian() {
		return Shrimp.getIsVegetarian();
	}

	public boolean getIsRice() {
		return Shrimp.getIsRice();
	}

	public boolean getIsShellfish() {
		return Shrimp.getIsShellfish();
	}

	public IngredientPortion combine(IngredientPortion other) {
		if (other == null) {
			return this; 
		} else if (!this.getName().equals(other.getName())) {
			throw new RuntimeException("Not the same ingregient"); 
		} else {
			RicePortion ShrimpPortion = new RicePortion(this.getAmount() + other.getAmount());
			return ShrimpPortion;
		}
	}	
}
