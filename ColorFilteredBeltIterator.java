package a6;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ColorFilteredBeltIterator implements Iterator<Plate> {
	private Belt belt;
	private int start_position;
	private Plate.Color color_filter;
	
	public ColorFilteredBeltIterator(Belt belt, int start_position, Plate.Color color_filter) {
		this.belt = belt;
		this.start_position = start_position;
		this.color_filter = color_filter;
	}

	// returns true if there is another plate located on the belt or false if the belt is completely empty
	public boolean hasNext() {
		for (int i = 0; i < belt.getSize(); i++) {
			if (belt.getPlateAtPosition(start_position) != null) {
				if (belt.getPlateAtPosition(start_position).getColor() ==  color_filter) {
					return true;
				}
			}
			this.start_position++;
		}
		return false;
	}

	// returns the next plate located on the belt
	public Plate next() {
		if (this.hasNext() == false) {
			throw new NoSuchElementException();
		}
		this.start_position++;
		return belt.getPlateAtPosition(start_position - 1);
	}
}
